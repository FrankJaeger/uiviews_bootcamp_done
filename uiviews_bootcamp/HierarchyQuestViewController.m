//
//  HierarchyQuestViewController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "HierarchyQuestViewController.h"
#import "SmallRectangleFriend.h"

@interface HierarchyQuestViewController ()

@end

@implementation HierarchyQuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hierarchyCheck {
    if(self.myRectangle.subviews) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
}

/**
 *
 *
 *
 */

- (IBAction)addViewToMe:(id)sender {
    
    [self performSelector:@selector(hierarchyCheck) withObject:self afterDelay:1.0f];
    /**
        * * * 6 * * *
        WRITE YOUR CODE HERE
        * * * * * * * 
     
        Your task is to create a view and add it to our lonely rectangle friend.
     
        Bonus points for creating a custom drawn UIView of class SmallRectangleFriend 
        and adding it to the center of our bigger rectangle friend.
     
     */
    
    SmallRectangleFriend *srf = [[ SmallRectangleFriend alloc] initWithFrame:CGRectMake((_myRectangle.frame.size.width/2)-25, (_myRectangle.frame.size.height/2)-25, 50, 50)];
    
    [ _myRectangle addSubview:srf ];
    
    SmallRectangleFriend *sview = [ _myRectangle.subviews firstObject ];
    
    [ UIView animateWithDuration:0.5 animations:^{
        sview.alpha =1;
    }];
    
        
}

/**
 *
 */

-(void)dealloc {
}

@end
