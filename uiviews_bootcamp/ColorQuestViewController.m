//
//  ColorQuestViewController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "ColorQuestViewController.h"

@interface ColorQuestViewController ()

@property (strong,nonatomic) UIColor *oldColor;

@end

@implementation ColorQuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.oldColor = self.myRectangle.backgroundColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)colorCheck {
    if (self.oldColor != self.myRectangle.backgroundColor) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
}

- (IBAction)colorMe:(id)sender {
    [self performSelector:@selector(colorCheck) withObject:self afterDelay:1.0f];
    /**
     * * * 5 * * *
     WRITE YOUR CODE HERE
     * * * * * * *
     
     Your task is to animate the change in color of your rectangle friend.
     
     Change his color to: red:126, green: 186, blue: 59
     
     */
    
    [ UIView animateWithDuration:0.5 animations:^{
        _myRectangle.backgroundColor = [ UIColor colorWithRed:126/255.f green:186/255. blue:59/255. alpha:1 ];
    }];
    
}

-(void)dealloc {
}

@end
