//
//  SmallRectangleFriend.m
//  uiviews_bootcamp
//
//  Created by dor on 15.04.2015.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "SmallRectangleFriend.h"

@implementation SmallRectangleFriend

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [ super initWithFrame:frame ];
    
    self.frame = frame;
    self.backgroundColor = [ UIColor blueColor ];
    self.alpha = 0;
    
    return self;
}

@end
