//
//  FrameQuestController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "FrameQuestController.h"

@interface FrameQuestController ()

@end

@implementation FrameQuestController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if(self.myRectangle.frame.size.width > 100) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
    
    
}

- (IBAction)resizeMe:(id)sender {
    [self.myRectangle addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:NULL];
    
    [UIView animateWithDuration:0.5 animations:^{
        _myRectangle.frame = CGRectMake(_myRectangle.center.x-100, _myRectangle.center.y-100, 200, 200 );
    }];
    
}

- (void)dealloc {
    [self.myRectangle removeObserver:self forKeyPath:@"frame"];
}

@end
